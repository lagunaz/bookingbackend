const mongoose = require('mongoose');
const { Schema } = mongoose
const FacultiesSchema = Schema({
    name: String,
    shortname: String
})

module.exports = mongoose.model('Faculties', FacultiesSchema)

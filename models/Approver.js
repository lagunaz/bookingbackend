const mongoose = require('mongoose')
const { Schema } = mongoose

const approverSchema = Schema({
  Name: String,
  User: [{ type: Schema.Types.ObjectId, ref: 'User' }]
})

module.exports = mongoose.model('Approver', approverSchema)

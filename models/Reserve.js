const mongoose = require('mongoose')
const { Schema } = mongoose
const reserveSchema = Schema({
  title: String,
  content: String,
  class: String,
  startDate: Date,
  endDate: Date,
  Room: { type: Schema.Types.ObjectId, ref: 'Room' },
  User: { type: Schema.Types.ObjectId, ref: 'User' },
  approver: [],
  status: { type: String, default: 'WAIT' }
}, {
  timestamps: true
})

module.exports = mongoose.model('Reserve', reserveSchema)
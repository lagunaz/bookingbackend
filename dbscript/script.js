/* eslint-disable space-before-function-paren */
const mongoose = require('mongoose')
const { ROLES } = require('../constant')
const User = require('../models/User')
const Faculties = require('../models/Faculties')
const Building = require('../models/Buildings')
const Reserve = require('../models/Reserve')
const Room = require('../models/Room')
const Approver = require('../models/Approver')

mongoose.connect('mongodb://localhost:27017/example')

async function clearAll() {
  await User.deleteMany({})
  await Faculties.deleteMany({})
  await Building.deleteMany({})
  await Reserve.deleteMany({})
  await Room.deleteMany({})
  await Approver.deleteMany({})
}

async function main() {
  await clearAll()

  const Informatics = new Faculties({ name: 'Informatics', shortname: 'IF' })
  const Medicine = new Faculties({ name: 'Medicine', shortname: 'MD' })
  const Science = new Faculties({ name: 'Science', shortname: 'SCI' })
  const Law = new Faculties({ name: 'Law', shortname: 'LW' })

  const Admin = new User({ username: 'admin@gmail.com', password: '1234', firstname: 'Admin', lastname: 'admin', faculty: Informatics, roles: [ROLES.ADMIN] })
  const AdminInformatics = new User({ username: 'adminIf@gmail.com', password: '1234', firstname: 'AdminIF', lastname: 'adminIF', faculty: Informatics, roles: [ROLES.ADMIN_FACULTY] })
  const Approver1 = new User({ username: 'approver1@gmail.com', password: '1234', firstname: 'Approver1', lastname: 'Approver1', faculty: Informatics, roles: [ROLES.APPROVER] })
  const Approver2 = new User({ username: 'approver2@gmail.com', password: '1234', firstname: 'Approver2', lastname: 'Approver2', faculty: Informatics, roles: [ROLES.APPROVER] })
  const Approver3 = new User({ username: 'approver3@gmail.com', password: '1234', firstname: 'Approver3', lastname: 'Approver3', faculty: Informatics, roles: [ROLES.APPROVER] })
  const UserTest = new User({ username: 'user@gmail.com', password: '1234', firstname: 'User', lastname: 'User', faculty: Informatics, roles: [ROLES.USER] })
  const UserMD = new User({ username: 'usermd@gmail.com', password: '1234', firstname: 'UserMD', lastname: 'UserMD', faculty: Medicine, roles: [ROLES.USER] })

  const buildingInformatice = new Building({ name: 'Informatics', faculty: Informatics, floor: 11, rooms: [] })
  const buildingMedicine = new Building({ name: 'Medicine', faculty: Medicine, floor: 8, rooms: [] })
  const buildingScience = new Building({ name: 'Science', faculty: Science, floor: 6, rooms: [] })
  const buildingLaw = new Building({ name: 'Law', faculty: Law, floor: 6, rooms: [] })

  const approve1 = new Approver({ Name: 'ลำดับการอนุมัติที่ 1', User: [Approver1, Approver2] })
  const approve2 = new Approver({ Name: 'ลำดับการอนุมัติที่ 2', User: [Approver2, Approver3] })
  const approve3 = new Approver({ Name: 'ลำดับการอนุมัติที่ 3', User: [Approver1, Approver2, Approver3] })

  const Room3C210 = new Room({ name: '3C210', capacity: 120, floor: 3, building: buildingInformatice, Approver: approve1 })
  const Room4C210 = new Room({ name: '4C210', capacity: 120, floor: 4, building: buildingInformatice, Approver: approve2 })
  const Room5C210 = new Room({ name: '5C210', capacity: 120, floor: 5, building: buildingInformatice, Approver: approve3 })

  buildingInformatice.rooms.push(Room3C210)
  buildingInformatice.rooms.push(Room4C210)
  buildingInformatice.rooms.push(Room5C210)

  await Informatics.save()
  await Medicine.save()
  await Science.save()
  await Law.save()

  await Admin.save()
  await AdminInformatics.save()
  await Approver1.save()
  await Approver2.save()
  await Approver3.save()
  await UserTest.save()
  await UserMD.save()

  await approve1.save()
  await approve2.save()
  await approve3.save()

  await Room3C210.save()
  await Room4C210.save()
  await Room5C210.save()

  await buildingInformatice.save()
  await buildingMedicine.save()
  await buildingScience.save()
  await buildingLaw.save()
}

main().then(() => {
  console.log('\'Mock DB\' done.')
})

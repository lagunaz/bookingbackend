const mongoose = require("mongoose")
const { ROLES } = require('../constant')
const User = require("../models/User")
const Faculties = require("../models/Faculties")

// random mock faculty for user
const randomFact = function(faculties){
    return faculties[Math.floor(Math.random() * faculties.length-1)]
}

// connect Mongo
mongoose.connect('mongodb://localhost:27017/example')

// clear product
async function clearUsers(){
    await User.deleteMany({})
}

// main
async function main(){
    await clearUsers()
    const faculties = await Faculties.find({}).exec()

    for(var i=0; i < 3; i++){
        const user = new User({
            username: `user${i+1}@gmail.com`,
            password: `user${i+1}`,
            firstname: `Username # ${i+1}`,
            lastname: `Lastname # ${i+1}`,
            faculty: randomFact(faculties),
            roles: [ROLES.USER]
        })
        await user.save()
    }
    const approver = new User({
        username: `approver@gmail.com`,
        password: `root`,
        firstname: `APPROVER # 1`,
        lastname: `APPROVER Lastname # 1`,
        faculty: randomFact(faculties),
        roles: [ROLES.APPROVER]
    })
    await approver.save()

    const adminUser = new User({
        username: `admin@gmail.com`,
        password: `root`,
        firstname: `ADMIN # 1`,
        lastname: `ADMIN # 1 Lastname`,
        faculty: randomFact(faculties),
        roles: [ROLES.ADMIN, ROLES.ADMIN_FACULTY, ROLES.USER]
    })
    await adminUser.save()
}

main().then(()=>{
    console.log('\'users\' done.');
})
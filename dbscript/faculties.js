const mongoose = require("mongoose")
const Faculties = require("../models/Faculties")

// connect Mongo
mongoose.connect('mongodb://localhost:27017/example')

// clear product
async function clearFaculties(){
    await Faculties.deleteMany({})
}

// mock data for all Faculty of book room
const facultyList = [
    {
        name: 'Informatics',
        shortname: 'IF'
    },
    {
        name: 'Medicine',
        shortname: 'MD'
    },
    {
        name: 'Science',
        shortname: 'SC'
    },
    {
        name: 'Law',
        shortname: 'LW'
    }
]

// Db Script for init MongoDB
async function main(){
    await clearFaculties()
    for(var i=0; i < facultyList.length; i++){
        const faculty = new Faculties({
            name: facultyList[i].name,
            shortname: facultyList[i].shortname
        })
        await faculty.save()
    }
    const facies = await Faculties.find({}).exec()
    console.log(facies);
}


main().then(()=>{
    console.log('\'Faculties\' done.');
})

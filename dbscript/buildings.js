const mongoose = require("mongoose")
const Buildings = require("../models/Buildings")
const Room = require('../models/Room')
const Faculties = require('../models/Faculties')


// random mock faculty for user
const requestFac = function(faculties, requestFac){
    return faculties.find(fac => fac.name == requestFac)
}

// connect Mongo
mongoose.connect('mongodb://localhost:27017/example')

// clear product
async function clearBuildings(){
    await Buildings.deleteMany({})
}


// mock data for all buildings of book room
const mockData = [
    {
        buildingsName: 'Informatics',
        faculty: 'Informatics',
        floor: 11 ,
        rooms: [
            {
                name: '3C01',
                capacity: 200,
                floor: 3
            },
            {
                name: '4C01',
                capacity: 300,
                floor: 4
            },
            {
                name: '5C01',
                capacity: 100,
                floor: 5
            }
        ]
    },
    {
        buildingsName: 'Medicine',
        faculty: 'Medicine',
        floor: 11 ,
        rooms: [
            {
                name: '3C01',
                capacity: 200,
                floor: 3
            },
            {
                name: '4C01',
                capacity: 300,
                floor: 4
            },
            {
                name: '5C01',
                capacity: 100,
                floor: 5
            }
        ]
    },
    {
        buildingsName: 'Science',
        faculty: 'Science',
        floor: 11 ,
        rooms: [
            {
                name: '3C01',
                capacity: 200,
                floor: 3
            },
            {
                name: '4C01',
                capacity: 300,
                floor: 4
            },
            {
                name: '5C01',
                capacity: 100,
                floor: 5
            }
        ]
    },
    {
        buildingsName: 'Law',
        faculty: 'Law',
        floor: 11 ,
        rooms: [
            {
                name: '3C01',
                capacity: 200,
                floor: 3
            },
            {
                name: '4C01',
                capacity: 300,
                floor: 4
            },
            {
                name: '5C01',
                capacity: 100,
                floor: 5
            }
        ]
    }
]


// Db Script for init MongoDB
async function main(){
    await clearBuildings()
    const faculties = await Faculties.find({}).exec()

    mockData.forEach( mockBuilding => {
        const building = new Buildings({
            name: mockBuilding.buildingsName,
            floor: mockBuilding.floor,
            faculty: requestFac(faculties, mockBuilding.faculty)
        })

        mockBuilding.rooms.forEach( mockRoom => {
            const room = new Room({
                name: mockRoom.name,
                capacity: mockRoom.capacity,
                floor: mockRoom.floor,
                building: building
            })
            building.rooms.push(room)
            room.save()
        })

        building.save()

    })

}

main().then(()=>{
    console.log('\'Buildings\' done.');
})
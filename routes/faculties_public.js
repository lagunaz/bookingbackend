const express = require('express')
const router = express.Router()
const Faculties = require('../models/Faculties')

const getFaculties = async function (req, res, next) {
  try {
    const faculties = await Faculties.find({}).exec()
    res.status(200).json(faculties)
  } catch (e) {
    return res.status(500).json({
      code: 500,
      status: `'can't get faculties' \nerror info: ${e.message}`
    })
  }
}

router.get('/', getFaculties)

module.exports = router

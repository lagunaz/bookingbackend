const express = require('express')
const router = express.Router()
const Buildings = require('../models/Buildings');

// function
const getBuildings = async function(req, res, next){
    try {
        const buildings = await Buildings.find({}).exec()
        res.status(200).json(buildings)
    } catch (e) {
        return res.status(500).json({
            code: 500,
            status: `'can't get buildings' \nerror info: ${e.message}`
        })
    }
}

// 'get' express
router.get('/', getBuildings) // get all buildings :D

module.exports = router
const express = require('express')
const router = express.Router()
const Approver = require('../models/Approver')

const getApprovers = async function (req, res, next) {
  try {
    const approver = await Approver.find({})
    res.status(200).json(approver)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getApprover = async function (req, res, next) {
  const id = req.params.id
  try {
    const approver = await Approver.findById(id)
    if (approver === null) {
      res.status(404).json({
        message: 'Approver not found!!'
      })
    }
    res.json(approver)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addApprover = async function (req, res, next) {
  const newApprover = new Approver({
    Name: req.body.Name,
    User: req.body.User
  })
  try {
    await newApprover.save()
    res.status(201).json(newApprover)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateApprover = async function (req, res, next) {
  const approverId = req.params.id
  try {
    const approver = await Approver.findById(approverId)
    approver.Name = req.body.Name
    approver.User = req.body.User
    await approver.save()
    return res.status(200).json(approver)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteApprover = async function (req, res, next) {
  const approverId = req.params.id
  try {
    await Approver.findByIdAndDelete(approverId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getApprovers)
router.get('/:id', getApprover)
router.post('/', addApprover)
router.put('/:id', updateApprover)
router.delete('/:id', deleteApprover)
module.exports = router

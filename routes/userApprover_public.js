const express = require('express')
const router = express.Router()
const User = require('../models/User')

const getApprovers = async function (req, res, next) {
  try {
    const approvers = await User.find({ roles: 'APPROVER' })
    res.status(200).json(approvers)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

router.get('/', getApprovers)
module.exports = router

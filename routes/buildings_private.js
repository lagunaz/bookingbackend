const express = require('express')
const router = express.Router()
const Buildings = require('../models/Buildings');

// function
const getBuildings = async function(req, res, next){
    try {
        const products = await Buildings.find({}).exec()
        res.status(200).json(products)
    } catch (e) {
        return res.status(500).json({
            code: 500,
            status: `'can't get products' \nerror info: ${e.message}`
        })
    }
}
const addBuildings = async function(req, res, next){
    /*
         @ TODO  Update Buildings
    */

    // const newProduct = new Buildings({
    //     name: req.body.name,
    //     price: req.body.price
    // })
    try {
        // await newProduct.save()
        // res.status(201).json(newProduct)
    } catch (e) {
        return res.status(500).json({
            code: 500,
            status: `'can't add building' \nerror info: ${e.message}`
        })
    }
}
const getBuildingsById = async function(req, res, next){
    try {
        const id = req.params.id
        const building = await Buildings.findById(id).exec()
        if(!building){
            return res.status(404).json({
                code: 404,
                status: `building not found \nerror info: ${e.message}`
            })
        }
        res.status(201).json(building)
    } catch (e) {
        return res.status(500).json({
            code: 500,
            status: `'can't get building ${req.params.id}' \nerror info: ${e.message}`
        })
    }
}
const updateBuildingsById = async function(req, res, next){ 
    try {
        const id = req.params.id
        const building = await Buildings.findById(id) 
        if(!building){
            return res.status(404).json({
                code: 404,
                status: `building not found \nerror info: ${e.message}`
            })
        }

        /*
            @ TODO  Update Buildings
        */

        // building.name = req.body.name
        // building.price = parseFloat(req.body.price)
        // await building.save()
        // return res.status(200).json(building)

    } catch (e) {
        return res.status(404).json({
            code: 404,
            status: `'can't update building ${req.params.id}' \nerror info: ${e.message}`
        })
    }
}
const deleteBuildingsById = async function(req, res, next){
    try {
        const id = req.params.id
        await Buildings.findByIdAndDelete(id)
        return res.status(201).send()
    } catch (e) {
        return res.status(500).json({
            code: 500,
            status: `'can't delete building ${req.params.id}' \nerror info: ${e.message}`
        })
    }
}



// 'get' express
router.get('/', getBuildings) // get all building :D
router.get('/:id', getBuildingsById) // get 'building' of 'products' by id

// 'post' express
router.post('/', addBuildings) // add building

// 'put' express
router.put('/:id', updateBuildingsById) // update building by id

// 'delete' express
router.delete('/:id', deleteBuildingsById)

module.exports = router
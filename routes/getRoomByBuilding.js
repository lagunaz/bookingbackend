const express = require('express')
const router = express.Router()
const Room = require('../models/Room')

const getRoomByBuilding = async function (req, res, next) {
  const buildingID = req.params.id
  try {
    const rooms = await Room.find({ building: buildingID }).exec()
    res.status(200).json(rooms)
  } catch (e) {
    return res.status(500).json({
      code: 500,
      status: `'can't get buildings' \nerror info: ${e.message}`
    })
  }
}

router.get('/:id', getRoomByBuilding)
module.exports = router

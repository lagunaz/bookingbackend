const express = require('express')
const router = express.Router()
const Reserve = require('../models/Reserve')
const mongoose = require('mongoose')

const getReserve = async function (req, res, next) {
  const id = req.params.id
  try {
    const reserve = await Reserve.find({ 'approver.User': mongoose.Types.ObjectId(id) }).populate('User').populate('Room')
    if (reserve === null) {
      res.status(404).json({
        message: 'Equipment not found!!'
      })
    }
    res.json(reserve)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.get('/:id', getReserve)
module.exports = router

const express = require('express')
const router = express.Router()
const Building = require('../models/Buildings')
const Room = require('../models/Room')

const getRooms = async function (req, res, next) {
  try {
    const rooms = await Room.find({}).populate('building').populate('Approver')
    res.status(200).json(rooms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getRoom = async function (req, res, next) {
  const id = req.params.id
  try {
    const room = await Room.findById(id).populate('building').populate('Approver')
    if (room === null) {
      res.status(404).json({
        message: 'Room not found!!'
      })
    }
    res.json(room)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addRooms = async function (req, res, next) {
  const room = new Room({
    name: req.body.name,
    capacity: req.body.capacity,
    floor: req.body.floor,
    building: req.body.building,
    Approver: req.body.Approver
  })
  try {
    console.log(room)
    const buildingFind = await Building.findById(room.building)
    buildingFind.rooms.push(room)
    await room.save()
    await buildingFind.save()
    res.status(201).json(room)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const deleteRooms = async function (req, res, next) {
  const idRoom = req.params.id
  const room = await Room.findById(idRoom)
  const BuildingFind = await Building.findById(room.building)
  try {
    await Room.findByIdAndDelete(idRoom)
    BuildingFind.rooms.pull(idRoom)
    await BuildingFind.save()
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const updateRooms = async function (req, res, next) {
  const roomId = req.params.id
  try {
    const room = await Room.findById(roomId)
    const BuildingFind = await Building.findById(room.building)
    console.log(room)
    room.name = req.body.name
    room.capacity = req.body.capacity
    room.floor = req.body.floor
    room.building = req.body.building
    room.Approver = req.body.Approver

    if (BuildingFind !== req.body.Building) {
      BuildingFind.rooms.pull(roomId)
      const newBuilding = await Building.findById(req.body.building)
      newBuilding.rooms.push(room)
      newBuilding.save()
    }
    await BuildingFind.save()
    await room.save()
    return res.status(200).json(room)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

router.put('/:id', updateRooms)

router.get('/', getRooms)
router.get('/:id', getRoom)
router.post('/', addRooms)
router.delete('/:id', deleteRooms)
module.exports = router

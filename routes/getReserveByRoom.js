const express = require('express')
const router = express.Router()
const Reserve = require('../models/Reserve')

const getReservesRoomId = async function (req, res, next) {
  const id = req.params.id
  const startDate = req.query.startDate
  const endDate = req.query.endDate
  try {
    const reserve = await Reserve.find({ Room: { _id: id }, status: ['Approved'], $or: [{ startDate: { $gte: startDate, $lt: endDate } }, { endDate: { $gte: startDate, $lt: endDate } }] }).populate('User').populate('Room')
    console.log(reserve)
    if (reserve === null) {
      res.status(404).json({
        message: 'Reserve not found!!'
      })
    }
    res.json(reserve)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.get('/:id', getReservesRoomId)

module.exports = router

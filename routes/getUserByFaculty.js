const express = require('express')
const router = express.Router()
const User = require('../models/User')
const mongoose = require('mongoose')

const getApprover = async function (req, res, next) {
  const id = req.params.id
  try {
    const user = await User.find({ faculty: mongoose.Types.ObjectId(id) }).populate('faculty')
    // console.log(user)
    if (user === null) {
      res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.json(user)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.get('/:id', getApprover)

module.exports = router

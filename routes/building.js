const express = require('express')
const router = express.Router()
const Building = require('../models/Buildings')
const Faculty = require('../models/Faculties')

const getBuildings = async function (req, res, next) {
  try {
    const buildings = await Building.find({}).populate('faculty')
    res.status(200).json(buildings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getBuilding = async function (req, res, next) {
  const id = req.params.id
  try {
    const building = await Building.findById(id).populate('faculty')
    if (building === null) {
      res.status(404).json({
        message: 'Equipment not found!!'
      })
    }
    res.json(building)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addBuildings = async function (req, res, next) {
  const facultyFind = await Faculty.findById(req.body.faculty)
  const newBuilding = new Building({
    name: req.body.name,
    floor: req.body.floor,
    faculty: facultyFind,
    Room: req.body.Room
  })
  try {
    await newBuilding.save()
    res.status(201).json(newBuilding)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateBuildings = async function (req, res, next) {
  const buildingId = req.params.id
  const facultyFind = await Faculty.findById(req.body.faculty)
  try {
    const building = await Building.findById(buildingId)
    building.name = req.body.name
    building.floor = req.body.floor
    building.faculty = facultyFind
    building.Room = req.body.Room
    await building.save()
    return res.status(200).json(building)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteBuildings = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    await Building.findByIdAndDelete(buildingId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getBuildings)
router.get('/:id', getBuilding)
router.post('/', addBuildings)
router.put('/:id', updateBuildings)
router.delete('/:id', deleteBuildings)

module.exports = router

const express = require('express')
const router = express.Router()
const Room = require('../models/Room')

const getRooms = async function (req, res, next) {
  try {
    const rooms = await Room.find({}).populate('building').populate('Approver')
    res.status(200).json(rooms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getRoom = async function (req, res, next) {
  const id = req.params.id
  try {
    const room = await Room.findById(id).populate('building').populate('Approver')
    if (room === null) {
      res.status(404).json({
        message: 'Room not found!!'
      })
    }
    res.json(room)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

router.get('/', getRooms)
router.get('/:id', getRoom)
module.exports = router

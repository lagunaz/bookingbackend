const express = require('express')
const router = express.Router()
const Reserve = require('../models/Reserve')
const Room = require('../models/Room')
const Approved = require('../models/Approver')
const mongoose = require('mongoose')

const getReserve = async function (req, res, next) {
  try {
    console.log(req.query)
    const startDate = req.query.startDate
    const endDate = req.query.endDate
    const reserve = await Reserve.find({
      $or: [{ startDate: { $gte: startDate, $lt: endDate } }, { endDate: { $gte: startDate, $lt: endDate } }]
    }).exec()
    res.status(200).json(reserve)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const addReserves = async function (req, res, next) {
  const room = await Room.findById(req.body.Room)
  const Appro = await Approved.findById(room.Approver)
  const approver = []
  Appro.User.forEach(element => {
    approver.push({ User: element._id, status: 'Waiting Approve' })
  })
  const newReserve = new Reserve({
    title: req.body.title,
    content: req.body.content,
    startDate: req.body.start,
    endDate: req.body.end,
    class: req.body.class,
    Room: req.body.Room,
    User: req.body.User,
    approver: approver
  })
  try {
    await newReserve.save()
    res.status(201).json(newReserve)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateReserves = async function (req, res, next) {
  const user = req.body.User
  const status = req.body.status
  const ReserveID = req.params.id

  const reserve = await Reserve.findById(ReserveID)
  const approver = JSON.parse(JSON.stringify(reserve.approver))
  approver.forEach(element => {
    if (element.User === user) {
      element.status = status
    }
    element.User = mongoose.Types.ObjectId(element.User)
  })

  console.log(approver)

  try {
    let check = 0
    approver.forEach(element => {
      if (element.status === 'Disapproved') {
        reserve.status = 'Disapproved'
      }
      if (element.status === 'Approved') {
        check++
      }

      if (approver.length === check) {
        reserve.status = 'Approved'
      }
    })
    reserve.approver = approver
    await reserve.save()
    return res.status(200).json(reserve)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

router.get('/', getReserve)
router.post('/', addReserves)
router.put('/:id', updateReserves)
module.exports = router
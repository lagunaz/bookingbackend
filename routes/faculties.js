const express = require('express')
const router = express.Router()
const Faculties = require('../models/Faculties');

// function
const getFaculties = async function(req, res, next){
    try {
        const faculties = await Faculties.find({}).exec()
        res.status(200).json(faculties)
    } catch (e) {
        return res.status(500).json({
            code: 500,
            status: `'can't get faculties' \nerror info: ${e.message}`
        })
    }
}


const addFaculties = async function (req, res, next) {
    const newFaculty = new Faculties({
        shortname: req.body.shortname,
        name: req.body.name
    })
    try {
        await newFaculty.save()
        res.status(201).json(newFaculty)
    } catch (err) {
        return res.status(500).send({
            message: err.message
        })
    }
}

const updateFaculties = async function (req, res, next) {
    const facId = req.params.id
    try {
        const faculty = await Faculties.findById(facId)
        faculty.shortname = req.body.shortname
        faculty.name = req.body.name
        await faculty.save()
        return res.status(200).json(faculty)
    } catch (err) {
        return res.status(404).send({
            message: err.message
        })
    }
}

const deleteFaculty = async function (req, res, next) {
    const facId = req.params.id
    try {
        await Faculties.findByIdAndDelete(facId)
        return res.status(200).send()
    } catch (err) {
        return res.status(404).send({ message: err.message })
    }
}


// 'get' express
router.get('/', getFaculties) // get all faculties :D
router.post('/', addFaculties)
router.put('/:id', updateFaculties)
router.delete('/:id', deleteFaculty)

module.exports = router
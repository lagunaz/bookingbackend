const ROLES = {
    ADMIN: 'ADMIN',
    ADMIN_FACULTY: 'ADMIN_FACULTY',
    USER: 'USER',
    APPROVER: 'APPROVER'
}

module.exports = {
    ROLES
}
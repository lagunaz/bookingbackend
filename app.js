var express = require('express');
const cors = require('cors')
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const mongoose = require('mongoose');

// @ All Routers
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const facultiesRouter = require('./routes/faculties')
const buildingsPrivateRouter = require('./routes/buildings_private')
const buildingsPublicRouter = require('./routes/buildings_public')
const authRouter = require('./routes/auth')
const approverPublicRouter = require('./routes/userApprover_public')
const getRoomByBuilding = require('./routes/getRoomByBuilding')
const reserve = require('./routes/reserve')
const buildingRouter = require('./routes/building')
const roomRouter = require('./routes/rooms')
const getReserveByUser = require('./routes/getReserveByUser')
const getFacultiesPublic = require('./routes/faculties_public')
const getRoomsPublic = require('./routes/rooms_public')
const getReserveByApprover = require('./routes/getReserveByApprover')
const getUserByFaculty = require('./routes/getUserByFaculty')
const getReserveByRoom = require('./routes/getReserveByRoom')

const dotenv = require('dotenv');
dotenv.config();
const { authMiddleware, authorizeMiddleware } = require('./helper/auth') 
const { ROLES } = require('./constant')

// const bodyParser = require('body-parser')

mongoose.connect('mongodb://localhost:27017/example')
var app = express();
app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const approverRouter = require('./routes/approver')






// @ App Uses 
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/approver', approverRouter)
app.use('/userApprover', approverPublicRouter)
app.use('/Room_Building', getRoomByBuilding)
app.use('/events', reserve)
app.use('/buildings', buildingRouter)
app.use('/rooms', roomRouter)
app.use('/reserveuser', getReserveByUser)
app.use('/facultyPublic', getFacultiesPublic)
app.use('/roomPublic', getRoomsPublic)
app.use('/reserveByApprover', getReserveByApprover)
app.use('/userByFaculty', getUserByFaculty)
app.use('/reserve_room', getReserveByRoom)
// app.use('/users', authMiddleware, authorizeMiddleware([ROLES.ADMIN, ROLES.ADMIN_FACULTY, ROLES.USER]), usersRouter);
app.use('/faculties', facultiesRouter)
// app.use('/faculties', authMiddleware, authorizeMiddleware([ROLES.ADMIN, ROLES.ADMIN_FACULTY, ROLES.USER]), facultiesRouter)
app.use('/buildings_private', authMiddleware, authorizeMiddleware([ROLES.ADMIN, ROLES.ADMIN_FACULTY]), buildingsPrivateRouter)
app.use('/buildings_public', buildingsPublicRouter)
app.use('/auth', authRouter)

module.exports = app;



